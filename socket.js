const express = require('express')
const http = require('http')
const socketIo = require('socket.io')

const app = express()
const server = http.createServer(app)
const io = socketIo(server, {
   cors: {
      origin: '*',
   }
});

app.set('port', 3001)

io.on('connection', socket => {
   console.log('Socket On')
   socket.on( 'new_consign', function( data ) {
      console.log(data);
      io.sockets.emit( 'show_consign', data);
    });

})

server.listen(app.get('port'), () => {
  console.log(`server on port ${app.get('port')}`);
});