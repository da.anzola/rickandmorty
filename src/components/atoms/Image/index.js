import PropTypes from 'prop-types'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'

const Image = styled.span`
  font-family: ${font('primary')};
  color: ${palette({ grayscale: 0 }, 1)};
`

Image.propTypes = {
  palette: PropTypes.string,
  reverse: PropTypes.bool,
}

Image.defaultProps = {
  palette: 'grayscale',
}

export default Image
