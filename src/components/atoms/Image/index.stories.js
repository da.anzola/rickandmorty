import React from 'react'
import { storiesOf } from '@storybook/react'
import Image from '.'

storiesOf('Image', module)
  .add('default', () => (
    <Image>Hello</Image>
  ))
  .add('reverse', () => (
    <Image reverse>Hello</Image>
  ))
