import PropTypes from 'prop-types'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'

const SubTitle = styled.h2`
  font-family: ${font('primary')};
  color: #4051b5;
  margin: 0 0 15px 0;
  font-size: 20px;
`

SubTitle.propTypes = {
  palette: PropTypes.string,
  reverse: PropTypes.bool,
}

SubTitle.defaultProps = {
  palette: 'grayscale',
}

export default SubTitle
