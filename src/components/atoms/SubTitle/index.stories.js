import React from 'react'
import { storiesOf } from '@storybook/react'
import SubTitle from '.'

storiesOf('SubTitle', module)
  .add('default', () => (
    <SubTitle>Hello</SubTitle>
  ))
  .add('reverse', () => (
    <SubTitle reverse>Hello</SubTitle>
  ))
