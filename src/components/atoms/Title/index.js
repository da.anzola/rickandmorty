import PropTypes from 'prop-types'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'

const Title = styled.h1`
  font-family: ${font('primary')};
  color: #4051b5;
  text-align: center;
  text-transform: uppercase;
  border-bottom: 2px solid #3f52b5;
  padding-bottom: 10px;
`

Title.propTypes = {
  palette: PropTypes.string,
  reverse: PropTypes.bool,
}

Title.defaultProps = {
  palette: 'grayscale',
}

export default Title
