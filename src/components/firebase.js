import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import config from 'config'

if (firebase.apps.length === 0) {
  firebase.initializeApp(config.firebase);
}

let db = firebase.firestore()
let auth = firebase.auth()

function getGoogleProvider() {
    return new firebase.auth.GoogleAuthProvider();
}

export default {
  firebase, db, auth, getGoogleProvider
}