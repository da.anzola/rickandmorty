import React from 'react'
import { storiesOf } from '@storybook/react'
import { Character } from 'components'

storiesOf('Character', module)
  .add('default', () => (
    <Character>Hello</Character>
  ))
  .add('reverse', () => (
    <Character reverse>Hello</Character>
  ))
