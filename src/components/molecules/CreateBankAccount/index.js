import React from 'react'
import PropTypes from 'prop-types'
import { Button, DialogTitle, Dialog, DialogContent, DialogContentText, DialogActions, TextField } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import Alert from '@material-ui/lab/Alert'
import { useUser } from '../../userContext'
import firebase from '../../firebase'

const CreateBankAccount = React.forwardRef((props, ref) => {
  const [open, setOpen] = React.useState(false);

  const [formError, setFormError] = React.useState(null)

  const [data, setData] = React.useState({
    number: (Math.floor(Math.random() * (9999 - 1000) ) + 1000),
    name: '',
    balance: '',
    uid: ''
  })

  const { user } = useUser();

  React.useEffect(() => {
    if (user) {
      setData({
        ...data,
        uid: user.uid
      })
    }
  }, [user]);

  const handleInputChange = (event) => {
    setData({
      ...data,
      [event.target.name] : event.target.value
    })
  }

  const handleClickOpen = () => {
    setData({
      ...data,
      number: (Math.floor(Math.random() * (9999 - 1000) ) + 1000)
    })
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const sendData = () => {
    setFormError(null)
    if (!data.name || !data.balance) {
      setFormError('Todos los datos son obligatorios.')
      return;
    }
    firebase.db.collection('bank-account').add(data).then(documentReference => {
      ref.current.addAccountData(data);
      handleClose();
    }).catch(error => {
      setFormError(error.message)
    })
    return;
  }

  return (
    <>
      <Button variant="contained" color="secondary" startIcon={<Add />} onClick={handleClickOpen} {...props}>
        Crear
      </Button>
      <Dialog aria-labelledby="simple-dialog-title" open={open}>
        <DialogTitle id="simple-dialog-title">Crear cuenta bancaria</DialogTitle>
        <DialogContent>
          { formError && <Alert severity="warning">{formError}</Alert> }
          <TextField margin="dense" variant="outlined" name="number" label="Número de cuenta" type="number" defaultValue={data.number} disabled fullWidth />
          <TextField margin="dense" variant="outlined" name="name" label="Nombre de la cuenta" type="text" onChange={handleInputChange} autoFocus fullWidth />
          <TextField margin="dense" variant="outlined" name="balance" label="Saldo" type="number" onChange={handleInputChange} fullWidth />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={sendData} color="primary">
            Crear
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
});

CreateBankAccount.propTypes = {
  reverse: PropTypes.bool,
  children: PropTypes.node,
}

export default CreateBankAccount
