import React from 'react'
import { storiesOf } from '@storybook/react'
import { CreateBankAccount } from 'components'

storiesOf('CreateBankAccount', module)
  .add('default', () => (
    <CreateBankAccount>Hello</CreateBankAccount>
  ))
  .add('reverse', () => (
    <CreateBankAccount reverse>Hello</CreateBankAccount>
  ))
