import React from 'react'
import PropTypes from 'prop-types'
import { Button, DialogTitle, Dialog, DialogContent, DialogContentText, DialogActions, TextField } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import Alert from '@material-ui/lab/Alert'
import { useUser } from '../../userContext'
import firebase from '../../firebase'
import io from 'socket.io-client'

const CreateConsign = React.forwardRef((props, ref) => {
  const socket = io('http://localhost:3001');

  const [open, setOpen] = React.useState(false);

  const [formError, setFormError] = React.useState(null)

  const [data, setData] = React.useState({
    number: '',
    name: '',
    balance: '',
    uid: ''
  })

  const { user } = useUser();

  React.useEffect(() => {
    if (user) {
      setData({
        ...data,
        uid: user.uid
      })
    }
  }, [user]);

  const handleInputChange = (event) => {
    setData({
      ...data,
      [event.target.name] : event.target.value
    })
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const sendData = () => {
    setFormError(null)
    if (!data.number || !data.name || !data.balance) {
      setFormError('Todos los datos son obligatorios.')
      return;
    }
    firebase.db.collection('bank-account').where('number', '==', parseInt(data.number)).get()
    .then(bankAccount => {
      if (bankAccount.empty) {
        setFormError(`La cuenta ${data.number} no existe.`)
      } else {
        bankAccount.forEach(account => {
          const accountData = account.data(); 
          firebase.db.collection('consign').add(data).then(documentReference => {
            let newAccountBalance = (parseInt(accountData.balance) + parseInt(data.balance));
            let dataAccount = {
              ...accountData,
              balance: newAccountBalance
            }
            firebase.db.collection('bank-account').doc(account.id).update({balance: newAccountBalance});
            ref.current.updateAccount(dataAccount);
            handleClose();
            socket.emit('new_consign', data);
          }).catch(error => {
            setFormError(error.message)
          })
        });
      }
    })
    .catch(error => {
      setFormError(error)
    });

    return;
  }

  return (
    <>
      <Button variant="contained" color="secondary" startIcon={<Add />} onClick={handleClickOpen} {...props}>
        Consignar
      </Button>
      <Dialog aria-labelledby="simple-dialog-title" open={open}>
        <DialogTitle id="simple-dialog-title">Realizar consignación</DialogTitle>
        <DialogContent>
          { formError && <Alert severity="warning">{formError}</Alert> }
          <TextField margin="dense" variant="outlined" name="number" label="Número de cuenta" type="number" onChange={handleInputChange} autoFocus fullWidth />
          <TextField margin="dense" variant="outlined" name="name" label="Motivo consignación" type="text" onChange={handleInputChange}  fullWidth />
          <TextField margin="dense" variant="outlined" name="balance" label="Valor" type="number" onChange={handleInputChange} fullWidth />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={sendData} color="primary">
            Consignar
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
});

CreateConsign.propTypes = {
  reverse: PropTypes.bool,
  children: PropTypes.node,
}

export default CreateConsign
