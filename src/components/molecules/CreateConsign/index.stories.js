import React from 'react'
import { storiesOf } from '@storybook/react'
import { CreateConsign } from 'components'

storiesOf('CreateConsign', module)
  .add('default', () => (
    <CreateConsign>Hello</CreateConsign>
  ))
  .add('reverse', () => (
    <CreateConsign reverse>Hello</CreateConsign>
  ))
