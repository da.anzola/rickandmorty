import React from 'react'
import { storiesOf } from '@storybook/react'
import { CreateCreditAccount } from 'components'

storiesOf('CreateCreditAccount', module)
  .add('default', () => (
    <CreateCreditAccount>Hello</CreateCreditAccount>
  ))
  .add('reverse', () => (
    <CreateCreditAccount reverse>Hello</CreateCreditAccount>
  ))
