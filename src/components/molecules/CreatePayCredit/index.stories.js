import React from 'react'
import { storiesOf } from '@storybook/react'
import { CreatePayCredit } from 'components'

storiesOf('CreatePayCredit', module)
  .add('default', () => (
    <CreatePayCredit>Hello</CreatePayCredit>
  ))
  .add('reverse', () => (
    <CreatePayCredit reverse>Hello</CreatePayCredit>
  ))
