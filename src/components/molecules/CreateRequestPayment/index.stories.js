import React from 'react'
import { storiesOf } from '@storybook/react'
import { CreateRequestPayment } from 'components'

storiesOf('CreateRequestPayment', module)
  .add('default', () => (
    <CreateRequestPayment>Hello</CreateRequestPayment>
  ))
  .add('reverse', () => (
    <CreateRequestPayment reverse>Hello</CreateRequestPayment>
  ))
