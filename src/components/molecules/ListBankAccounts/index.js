import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import Alert from '@material-ui/lab/Alert'
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Snackbar } from '@material-ui/core'
import firebase from '../../firebase'
import { useUser } from '../../userContext'
import io from 'socket.io-client'

const Wrapper = styled.div`
  margin-top: 20px;
`

const StyledTableCell = withStyles((theme) => ({

  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
  },
});

const AlertCustom = styled(Alert)`
  margin-bottom: 20px;
`

const ListBankAccounts = React.forwardRef((props, ref) => {
  const socket = io('http://localhost:3001');

  const { user } = useUser();

  const [listError, setListError] = React.useState(null)

  const [tableData, setTableData] = React.useState([])

  const [loadedData, setLoadedData] = React.useState(false)

  const [openAlert, setOpenAlert] = React.useState(false)

  const [alertMessage, setAlertMessage] = React.useState('')

  const getAccounts = () => {
    firebase.db.collection('bank-account')
    .where('uid', '==', user.uid).get()
    .then(snapshot => {
      if (snapshot.empty) {
        setListError('No haz creado cuentas bancarias')
      } else {
        snapshot.forEach(doc => {
          addAccount(doc.data());
        });
      }
      if (!loadedData) {
        setLoadedData(true);
      }
    })
    .catch(error => {
      setListError(error)
    });
  }

  React.useImperativeHandle(ref, () => ({
    addAccountData(data) {
      setTableData(tableData => [data, ...tableData]);
      handleOpenAlert('Cuenta agregada correctamente.')
      setListError(null)
    },
    updateAccount(data){
      tableData.forEach((row, index) => {
        if (row.number == data.number) {
          tableData[index] = data
        }
      });
      setTableData([]);
      setTableData(tableData);
      handleOpenAlert('Operación realizada correctamente.')
    }
  }));

  const addAccount = (data) => {
    setTableData(tableData => [...tableData, data]);
  }

  const handleOpenAlert = (message) => {
    setOpenAlert(true)
    setAlertMessage(message)
  }

  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenAlert(false);
  };

  React.useEffect(() => {
    if (user) {
      getAccounts();
    }
  }, [user])

  React.useEffect(() => {
    if (user && tableData) {
      socket.on('show_consign',  data => {
        if (data.uid != user.uid) {
          let alert = false;
          tableData.forEach((row, index) => {
            if (row.number == data.number) {
              alert = true;
              tableData[index].balance = parseInt(row.balance) + parseInt(data.balance);
            }
          });
          if (alert) {
            setTableData([]);
            setTableData(tableData);
            setOpenAlert(true)
            setAlertMessage(`Recibiste una consignación (${data.name}) de $${data.balance} a tu cuenta ${data.number}`)
          }
        }
      });

    }
  }, [user, loadedData])

  const classes = useStyles();
  return (
    <Wrapper>
      { listError && <AlertCustom severity="warning">{listError}</AlertCustom> }
      <TableContainer component={Paper} >
        <Table aria-label="customized table" className={classes.table}>
          <TableHead>
            <TableRow>
              <StyledTableCell>Número</StyledTableCell>
              <StyledTableCell>Nombre</StyledTableCell>
              <StyledTableCell>Saldo</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {tableData.map((row) => (
              <StyledTableRow key={row.number}>
                <StyledTableCell>{row.number}</StyledTableCell>
                <StyledTableCell>{row.name}</StyledTableCell>
                <StyledTableCell>{row.balance}</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
        <Alert severity="success">{alertMessage}</Alert>
      </Snackbar>
    </Wrapper>
  )
});

ListBankAccounts.propTypes = {
  reverse: PropTypes.bool,
  children: PropTypes.node,
}

export default ListBankAccounts
