import React from 'react'
import { storiesOf } from '@storybook/react'
import { ListBankAccounts } from 'components'

storiesOf('ListBankAccounts', module)
  .add('default', () => (
    <ListBankAccounts>Hello</ListBankAccounts>
  ))
  .add('reverse', () => (
    <ListBankAccounts reverse>Hello</ListBankAccounts>
  ))
