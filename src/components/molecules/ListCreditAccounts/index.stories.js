import React from 'react'
import { storiesOf } from '@storybook/react'
import { ListCreditAccounts } from 'components'

storiesOf('ListCreditAccounts', module)
  .add('default', () => (
    <ListCreditAccounts>Hello</ListCreditAccounts>
  ))
  .add('reverse', () => (
    <ListCreditAccounts reverse>Hello</ListCreditAccounts>
  ))
