import React from 'react'
import PropTypes from 'prop-types'
import { Button, ButtonGroup } from '@material-ui/core'
import { Face, Close } from '@material-ui/icons'
import { Link } from 'components'
import { useUser } from '../../userContext'

const LinkRef = React.forwardRef((props, ref) => <Link {...props} />)

const UserActions = ({ children, ...props }) => {
  const { user, logout } = useUser();
  return (
    <ButtonGroup>
      <Button component={LinkRef} variant="contained" color="primary" endIcon={<Face />} to="/profile" >
        {user.name}
      </Button>
      <Button variant="contained" color="secondary" onClick={logout} >
        <Close />
      </Button>
    </ButtonGroup>
  )
}

UserActions.propTypes = {
  reverse: PropTypes.bool,
  children: PropTypes.node,
}

export default UserActions
