import React from 'react'
import { storiesOf } from '@storybook/react'
import { UserActions } from 'components'

storiesOf('UserActions', module)
  .add('default', () => (
    <UserActions>Hello</UserActions>
  ))
  .add('reverse', () => (
    <UserActions reverse>Hello</UserActions>
  ))
