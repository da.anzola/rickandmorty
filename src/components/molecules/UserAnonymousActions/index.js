import React from 'react'
import PropTypes from 'prop-types'
import { Button, ButtonGroup } from '@material-ui/core'
import { Link } from 'components'

const LinkRef = React.forwardRef((props, ref) => <Link {...props} />)

const UserAnonymousActions = ({ children, ...props }) => {
  return (
    <ButtonGroup>
      <Button component={LinkRef} variant="contained" size="medium" color="primary" to="/login">
        Login
      </Button>
      <Button component={LinkRef} size="medium" color="secondary" to="/register">
        Sign in
      </Button>
    </ButtonGroup>
  )
}

UserAnonymousActions.propTypes = {
  reverse: PropTypes.bool,
  children: PropTypes.node,
}

export default UserAnonymousActions
