import React from 'react'
import { storiesOf } from '@storybook/react'
import { UserAnonymousActions } from 'components'

storiesOf('UserAnonymousActions', module)
  .add('default', () => (
    <UserAnonymousActions>Hello</UserAnonymousActions>
  ))
  .add('reverse', () => (
    <UserAnonymousActions reverse>Hello</UserAnonymousActions>
  ))
