import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'
import Alert from '@material-ui/lab/Alert'
import { ButtonGroup } from '@material-ui/core'
import { SubTitle, CreateBankAccount, ListBankAccounts, CreateConsign, CreateRequestPayment } from 'components'

const Wrapper = styled.div`
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  margin-bottom: 50px;
`
const ActionsRight = styled.div`
  float: right;
`

const BankAccounts = (props) => {

  const childRef = React.useRef();

  return (
    <Wrapper>
      <ActionsRight>
        <ButtonGroup color="secondary" aria-label="outlined secondary button group">
          <CreateRequestPayment size="small" ref={childRef} />
          <CreateConsign size="small" ref={childRef} />
          <CreateBankAccount size="small" ref={childRef} />
        </ButtonGroup>
      </ActionsRight>
      <SubTitle>Cuentas bancarias</SubTitle>
      <Alert severity="info">Puedes crear cuentas de ahorro o corriente.</Alert>
      <ListBankAccounts ref={childRef} />
    </Wrapper>
  )
}

BankAccounts.propTypes = {
  reverse: PropTypes.bool,
}

export default BankAccounts
