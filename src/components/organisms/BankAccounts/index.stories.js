import React from 'react'
import { storiesOf } from '@storybook/react'
import { BankAccounts } from 'components'

storiesOf('BankAccounts', module)
  .add('default', () => (
    <BankAccounts />
  ))
  .add('reverse', () => (
    <BankAccounts reverse />
  ))
