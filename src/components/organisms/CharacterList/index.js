import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'
import { Character } from 'components';
import { Grid } from '@material-ui/core'

const Wrapper = styled.div`
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
`

const CharacterList = (props) => {

  const [users, setUsers] = React.useState([])
  
  React.useEffect(() => {
    getData();
  }, [])

  const getData = async () => {
    const data = await fetch('https://rickandmortyapi.com/api/character')
    const users = await data.json()
    console.log(users['results'])
    setUsers(users['results'])
  }

  return (
    <Wrapper {...props}>
      <Grid container spacing={5}>
        { users.map(item => (
          <Grid key={item.id} container item md={4} sm={6}>
            <Character {...item}/>
          </Grid>
        )) }
      </Grid>
    </Wrapper>
  )
}

CharacterList.propTypes = {
  reverse: PropTypes.bool,
}

export default CharacterList
