import React from 'react'
import { storiesOf } from '@storybook/react'
import { CharacterList } from 'components'

storiesOf('CharacterList', module)
  .add('default', () => (
    <CharacterList />
  ))
  .add('reverse', () => (
    <CharacterList reverse />
  ))
