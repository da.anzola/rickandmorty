import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { font, palette } from 'styled-theme'
import Alert from '@material-ui/lab/Alert'
import { ButtonGroup } from '@material-ui/core'
import { SubTitle, CreateCreditAccount, CreatePayCredit, ListCreditAccounts } from 'components'

const Wrapper = styled.div`
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  margin-bottom: 50px;
`
const ActionsRight = styled.div`
  float: right;
`

const CreditAccounts = (props) => {

  const childRef = React.useRef();

  return (
    <Wrapper>
      <ActionsRight>
        <ButtonGroup color="secondary" aria-label="outlined secondary button group">
          <CreatePayCredit size="small" ref={childRef} />
          <CreateCreditAccount size="small" ref={childRef} />
        </ButtonGroup>
      </ActionsRight>
      <SubTitle>Préstamos</SubTitle>
      <Alert severity="info">Puedes solicitar préstamos para casa o carro.</Alert>
      <ListCreditAccounts ref={childRef} />
    </Wrapper>
  )
}

CreditAccounts.propTypes = {
  reverse: PropTypes.bool,
}

export default CreditAccounts
