import React from 'react'
import { storiesOf } from '@storybook/react'
import { CreditAccounts } from 'components'

storiesOf('CreditAccounts', module)
  .add('default', () => (
    <CreditAccounts />
  ))
  .add('reverse', () => (
    <CreditAccounts reverse />
  ))
