import React from 'react'
import styled from 'styled-components'
import { Container } from '@material-ui/core'
import { Paragraph, Block } from 'components'

const FooterContent = styled(Paragraph)`
  color: white;
  text-align: center;
  padding: 15px;
`

const today = new Date();
const year = today.getFullYear();

const Footer = (props) => {
  return (
    <Block opaque reverse {...props}>
      <Container>
        <FooterContent>Powered by Cris - Copyright {year}</FooterContent>
      </Container>
    </Block>
  )
}

export default Footer
