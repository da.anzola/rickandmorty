import React from 'react'
import styled from 'styled-components'
import { Container } from '@material-ui/core'
import { IconLink, UserActions, UserAnonymousActions, Block } from 'components'
import { useUser } from '../../userContext'


const Wrapper = styled(Block)`
  padding: 10px 0;
`

const Actions = styled.div`
  float: right;
`

const Header = (props) => {
//
  const { user, userLoading } = useUser();

  return (
    <Wrapper opaque reverse {...props}>
      <Container>
        <IconLink to="/" icon="arc" height={100} />
        <Actions>
          { !userLoading && !user && <UserAnonymousActions /> }
          { !userLoading && user && <UserActions /> }
        </Actions>
      </Container>
    </Wrapper>
  )
}

export default Header
