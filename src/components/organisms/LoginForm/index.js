import React from 'react'
import styled from 'styled-components'
import { Button, TextField } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert';
import firebase from '../../firebase'
import { useUser } from '../../userContext'

const Wrapper = styled.div`
  margin: auto;
  max-width: 400px;
`

const FormElement = styled.div`
  margin: 10px 0;
`

const LoginButton = styled(Button)`
  width: 100%;
`

const LoginTextField = styled(TextField)`
  width: 100%;
`

const LoginForm = () => {

  const [loginError, setLoginError] = React.useState(null)

  const [data, setData] = React.useState({
    mail: '',
    pass: ''
  })

  const [profileData, setProfileData] = React.useState({
    id: '',
    name: '',
    image: '',
    location: ''
  });

  const handleInputChange = (event) => {
    setData({
      ...data,
      [event.target.name] : event.target.value
    })
  }

  const sendData = (event) => {
    event.preventDefault()
    setLoginError(null);
    firebase.auth.signInWithEmailAndPassword(data.mail, data.pass)
    .then((result) => {
        loginProfile(result.user.uid);
    })
    .catch((error) => {
      setLoginError(error.message);
    });
  }

  const { login } = useUser();

  const googleLogin = () => {
    setLoginError(null);
    let provider = firebase.getGoogleProvider();
    firebase.auth.signInWithPopup(provider)
    .then((result) => {
      loginProfile(result.user.uid)
    })
    .catch((error) => {
      setLoginError(error.message)
    });
  }

  const loginProfile = (uid) => {
    let profile = firebase.db.collection('profile');
    let query = profile.where('uid', '==', uid).get()
    .then(snapshot => {
      if (snapshot.empty) {
        setLoginError('El usuario no existe.')
        return;
      }
      snapshot.forEach(doc => {
        login(doc.data());
        return;
      });
    })
    .catch(error => {
      setLoginError(error)
    });
  }

  return (
    <Wrapper>
      {loginError ? (<Alert onClose={() => setLoginError(null)} severity="warning">{loginError}</Alert>) : null}
      <form noValidate autoComplete="off" onSubmit={sendData}>
        <FormElement>
          <LoginButton variant="contained" color="default" type="button" onClick={googleLogin}>
            Ingreso con Google
          </LoginButton>
        </FormElement>
        <FormElement>
          <LoginTextField name="mail" label="Correo electrónico" variant="outlined" onChange={handleInputChange} />
        </FormElement>
        <FormElement>
          <LoginTextField name="pass" label="Contraseña" type="password" variant="outlined" onChange={handleInputChange} />
        </FormElement>
        <FormElement>
          <LoginButton variant="contained" color="secondary" type="submit">
            Iniciar sesión
          </LoginButton>
        </FormElement>
      </form>
    </Wrapper>
  )
}

export default LoginForm
