import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import config from 'config'
import styled from 'styled-components'

const FormElement = styled.div`
  margin: 10px 0;
`
const FormField = styled(TextField)`
  width: 100%;
`

const RegisterDialog = (props) => {
  const [open, setOpen] = React.useState(true);
  const [registerError, setRegisterError] = React.useState(null);
  const [user, setUser] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  React.useEffect(() => {
    if (firebase.apps.length === 0) {
      firebase.initializeApp(config.firebase);
    }

    firebase.auth().createUserWithEmailAndPassword(`user${props.id}@gmail.es`, '112233')
    .then((user) => {
      // Signed in
      // ...
      setUser(user);
      console.log(user);
      console.log('Registrado');
      
      firebase.firestore().collection('usuarios').add({'id': props.id, 'name': props.name, 'uid': user.uid})
        .then(documentReference => {
         console.log('document reference ID', documentReference.id)
        })
        .catch(error => {
          console.log(error.message)
        })
    })
    .catch((error) => {
      var errorMessage = error.message;
      setRegisterError(errorMessage);
      console.log(error);
      console.log('No registrado');
      // ..
    });
  }, []);


  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{ user ? 'Usuario creado!' : props.name}</DialogTitle>
        <DialogContent>
          { !user && <DialogContentText>
            { registerError || 'Creando la cuenta de usuario...' }
          </DialogContentText> }
          {registerError && 
            <div>
              <FormElement>
                <FormField disabled id="filled-disabled" label="Correo" defaultValue={`user${props.id}@gmail.es`} variant="filled" />
              </FormElement>
              <FormElement>
                <FormField disabled id="filled-disabled" label="Contraseña" defaultValue="112233" variant="filled" />
              </FormElement>
            </div>}
          { user && 
            <div>
              <FormElement>
                <FormField disabled id="filled-disabled" label="Correo" defaultValue={user.user.email} variant="filled" />
              </FormElement>
              <FormElement>
                <FormField disabled id="filled-disabled" label="Contraseña" defaultValue="112233" variant="filled" />
              </FormElement>
            </div>}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

RegisterDialog.propTypes = {
  reverse: PropTypes.bool,
}

export default RegisterDialog
