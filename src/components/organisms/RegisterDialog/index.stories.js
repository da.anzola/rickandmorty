import React from 'react'
import { storiesOf } from '@storybook/react'
import { RegisterDialog } from 'components'

storiesOf('RegisterDialog', module)
  .add('default', () => (
    <RegisterDialog />
  ))
  .add('reverse', () => (
    <RegisterDialog reverse />
  ))
