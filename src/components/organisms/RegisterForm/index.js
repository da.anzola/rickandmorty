import React from 'react'
import styled from 'styled-components'
import { Button, TextField } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert';
import Autocomplete from '@material-ui/lab/Autocomplete';
import firebase from '../../firebase'
import { useUser } from '../../userContext'

const Wrapper = styled.div`
  margin: auto;
  max-width: 400px;
`

const FormElement = styled.div`
  margin: 10px 0;
`

const RegisterButton = styled(Button)`
  width: 100%;
`

const LoginTextField = styled(TextField)`
  width: 100%;
`

const RegisterForm = () => {

  const [registerError, setRegisterError] = React.useState(null)

  const [userData, setUserData] = React.useState({
    character: '',
    mail: '',
    pass: '',
  })

  const [profileData, setProfileData] = React.useState({
    id: '',
    name: '',
    image: '',
    location: ''
  });

  const [characters, setCharacters] = React.useState([])

  const handleInputChange = (event) => {
    setUserData({
        ...userData,
        [event.target.name] : event.target.value
    })
  }

  const handleCharacterSelect = (event, value) => {
    if (value !== 'undefined') {
      setUserData({
        ...userData,
        character: value
      })
    }
    let profile = characters.find(character => character.name === value);
    if (profile) {
      setProfileData({
        id: profile.id,
        name: profile.name,
        image: profile.image,
        location: profile.location.name
      });
    }
  }

  const handleCharacterChange = (event) => {
    getCharacters(event.target.value)
  }

  const sendData = (event) => {
    event.preventDefault()
    if ( !userData.character || !userData.mail || !userData.pass ) {
      setRegisterError('Todos los campos son obligatorios.')
      return false;
    }
    if ( !profileData.id ) {
      setRegisterError('El personaje seleccionado no existe.')
      return false;
    }

    setRegisterError(null);
    firebase.auth.createUserWithEmailAndPassword(userData.mail, userData.pass)
    .then((result) => {
      createProfile(result);
    })
    .catch((error) => {
      setRegisterError(error.message);
    });
  }

  const googleRegister = () => {
    if ( !userData.character ) {
      setRegisterError('Selecciona el personaje para hacer el registro con Google.')
      return false;
    }
    setRegisterError(null);
    let provider = firebase.getGoogleProvider();
    firebase.auth.signInWithPopup(provider)
    .then((result) => {
      if ( !result.additionalUserInfo.isNewUser ){
        let profile = firebase.db.collection('profile');
        let query = profile.where('uid', '==', result.user.uid).get()
        .then(snapshot => {
          if (snapshot.empty) {
            createProfile(result, true);
          } else {
            createProfile(result);
          }
        })
        .catch(error => {
          setRegisterError(error)
        });
      } else {
        createProfile(result);
      }
    })
    .catch((error) => {
      setRegisterError(error.message)
    });
  }

  const { login } = useUser();

  const createProfile = (result, forceNew = false) => {
    const aditonalData = {
      uid: result.user.uid,
      email: result.user.email
    }
    const profile = {...profileData, ...aditonalData }
    if ( result.additionalUserInfo.isNewUser || forceNew ) {
      firebase.db.collection('profile').add(profile).then(documentReference => {
        login(profile);
      }).catch(error => {
        setRegisterError(error.message)
      })
    } else {
      login(profile);
    }
  }

  
  React.useEffect(() => {
    getCharacters();
  }, [])

  const getCharacters = async (name = '') => {
    const data = await fetch('https://rickandmortyapi.com/api/character' + (name && '?name=' + name))
    const characters = await data.json()
    if (characters['results']) {
      setCharacters(characters['results'])
    } else {
      setCharacters([])
    }
  }

  return (
    <Wrapper>
      {registerError ? (<Alert onClose={() => setRegisterError(null)} severity="warning">{registerError}</Alert>) : null}
      <form noValidate autoComplete="off" onSubmit={sendData}>
        <FormElement>
          <Autocomplete
            options={characters}
            getOptionLabel={(option) => option.name}
            onInputChange={handleCharacterSelect}
            renderInput={(params) => <LoginTextField {...params} name="character" label="Personaje" variant="outlined" onChange={handleCharacterChange} />}
          />
        </FormElement>
        <FormElement>
          <RegisterButton variant="contained" color="default" type="button" onClick={googleRegister}>
            Registro con Google
          </RegisterButton>
        </FormElement>
        <FormElement>
          <LoginTextField name="mail" label="Correo electrónico" variant="outlined" onChange={handleInputChange} />
        </FormElement>
        <FormElement>
          <LoginTextField name="pass" label="Contraseña" type="password" variant="outlined" onChange={handleInputChange} />
        </FormElement>
        <FormElement>
          <RegisterButton variant="contained" color="secondary" type="submit">
            Registrar
          </RegisterButton>
        </FormElement>
      </form>
    </Wrapper>
  )
}

export default RegisterForm
