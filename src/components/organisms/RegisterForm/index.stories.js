import React from 'react'
import { storiesOf } from '@storybook/react'
import { RegisterForm } from 'components'

storiesOf('RegisterForm', module)
  .add('default', () => (
    <RegisterForm />
  ))
  .add('reverse', () => (
    <RegisterForm reverse />
  ))
