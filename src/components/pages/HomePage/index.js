// https://github.com/diegohaz/arc/wiki/Atomic-Design
import React from 'react'
import {Button, Container} from '@material-ui/core';

import {
  CharacterList, PageTemplate, Header, Footer,
} from 'components'

const HomePage = () => {
  return (
    <PageTemplate
      header={<Header/>}
      footer={<Footer/>}
    >
      <Container>
        <CharacterList/>
      </Container>
    </PageTemplate>
  )
}

export default HomePage
