import React from 'react'
import { Container } from '@material-ui/core'

import {
  PageTemplate, Header, Footer, LoginForm, Title
} from 'components'

const LoginPage = () => {

  return (
    <PageTemplate
    header={<Header/>}
    footer={<Footer/>}
    >
      <Container>
        <Title>Iniciar sesión</Title>
        <LoginForm/>
      </Container>
    </PageTemplate>
  )
}

export default LoginPage
