// https://github.com/diegohaz/arc/wiki/Atomic-Design
import React from 'react'
import { Container, Grid } from '@material-ui/core';
import { useUser } from '../../userContext'

import {
  Character, PageTemplate, Header, Footer, Title, BankAccounts, CreditAccounts
} from 'components'

const ProfilePage = () => {

  const { user } = useUser();

  return (
    <PageTemplate
      header={<Header/>}
      footer={<Footer/>}
    >
      <Container>
        <Title>Mi perfil</Title>
        <Grid container spacing={3}>
          <Grid item xs={12} md={3}>
            {user && <Character {...user} />}
          </Grid>
          <Grid item xs={12} md={9}>
            <BankAccounts />
            <CreditAccounts />
          </Grid>
        </Grid>
      </Container>
    </PageTemplate>
  )
}

export default ProfilePage
