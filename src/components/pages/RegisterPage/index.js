import React from 'react'
import { Container } from '@material-ui/core'

import {
  PageTemplate, Header, Footer, RegisterForm, Title
} from 'components'

const RegisterPage = () => {

  return (
    <PageTemplate
    header={<Header/>}
    footer={<Footer/>}
    >
      <Container>
        <Title>Registrar usuario</Title>
        <RegisterForm/>
      </Container>
    </PageTemplate>
  )
}

export default RegisterPage
