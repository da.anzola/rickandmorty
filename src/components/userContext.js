import React from 'react';
 
const UserContext = React.createContext();
 
export function UserProvider(props) {
  const [user, setUser] = React.useState(null);
  const [userLoading, setUserLoading] = React.useState(true);

  React.useEffect(() => {
    let storageUser = localStorage.getItem('user');
    if (storageUser) {
      try {
        let userObject = JSON.parse(storageUser);
        setUser(userObject)
      } catch (e) {
        
      }
    }
    setUserLoading(false);
  }, [])

  function logout() {
    console.log('logout')
    setUser(null);
    localStorage.removeItem('user');
  }

  function login(data) {
    setUser(data);
    localStorage.setItem('user', JSON.stringify(data));
  }

  const value = React.useMemo(() => {
    return ({
      user,
      userLoading,
      logout,
      login
    })
  }, [user, userLoading])

  return <UserContext.Provider value={value} {...props} />
}

export function useUser() {
    const context = React.useContext(UserContext);
    if (!context) {
        throw new Error('useUser debe estar dentro del proveedor UserContext')
    }
    return context;
}