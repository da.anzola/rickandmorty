const merge = require('lodash/merge')

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    isDev: process.env.NODE_ENV !== 'production',
    basename: process.env.PUBLIC_PATH,
    isBrowser: typeof window !== 'undefined',
    firebase: {
      apiKey: "AIzaSyC9UK7BSLIQFkQhVbvJnhUvUImT1Ow-E54",
      projectId: "rickandmorty-ce048",
      databaseURL: "https://rickandmorty-ce048.firebaseio.com",
      authDomain: "rickandmorty-ce048.firebaseapp.com"
    }
  },
  test: {},
  development: {},
  production: {},
}

module.exports = merge(config.all, config[config.all.env])
